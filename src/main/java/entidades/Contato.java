package entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entidade
 * User: giovanni
 * Date: 15/05/12
 * Time: 15:40
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class Contato implements Serializable {
    private static final long serialVersionUID = 2L;
    private String nome;
    private String endereco;
    private String email;
    private String telefone;
    private String celular;
    private List<NaveEspacial> naves = new ArrayList<NaveEspacial>();

    public Contato() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public List<NaveEspacial> getNaves() {
        return naves;
    }

    public void setNaves(List<NaveEspacial> naves) {
        this.naves = naves;
    }

    @Override
    public String toString() {
        return "Contato{" +
                "nome='" + nome + '\'' +
                ", endereco='" + endereco + '\'' +
                ", email='" + email + '\'' +
                ", telefone='" + telefone + '\'' +
                ", celular='" + celular + '\'' +
                ", naves=" + naves +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contato)) return false;

        Contato contato = (Contato) o;

        if (celular != null ? !celular.equals(contato.celular) : contato.celular != null) return false;
        if (email != null ? !email.equals(contato.email) : contato.email != null) return false;
        if (endereco != null ? !endereco.equals(contato.endereco) : contato.endereco != null) return false;
        if (nome != null ? !nome.equals(contato.nome) : contato.nome != null) return false;
        if (telefone != null ? !telefone.equals(contato.telefone) : contato.telefone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nome != null ? nome.hashCode() : 0;
        result = 31 * result + (endereco != null ? endereco.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (telefone != null ? telefone.hashCode() : 0);
        result = 31 * result + (celular != null ? celular.hashCode() : 0);
        return result;
    }
}
