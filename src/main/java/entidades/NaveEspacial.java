package entidades;

import java.io.Serializable;

/**
 * Entidade pessoa
 * User: giovanni
 * Date: 16/05/12
 * Time: 11:38
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class NaveEspacial implements Serializable {
    private String nome;
    private String cor;

    public NaveEspacial(String nome, String cor) {
        this.nome = nome;
        this.cor = cor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    @Override
    public String toString() {
        return "NaveEspacial{" +
                "nome='" + nome + '\'' +
                ", cor='" + cor + '\'' +
                '}';
    }
}
