import dao.ContatoDAO;
import dao.ContatoXMLDAO;
import dao.DAO;
import dao.PrevalenceDAO;
import entidades.Contato;
import entidades.NaveEspacial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Classe Principal
 * User: giovanni
 * Date: 16/05/12
 * Time: 10:23
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class Main {
    private DAO dao;

    public static void main(String[] args) {
        new Main().execute();

    }

    private void execute() {
        String resposta = null;
        do {
            try {
                System.out.println("Que timpo de serializacao você quer utilizar? " +
                        "\n 1) JAVA (salvo na pasta BASE)" +
                        "\n 2) XML (salvo na pasta XML) ");
                resposta = lerTeclado();
                if (resposta.equals("1")) {
                    dao = new ContatoDAO();
                } else if (resposta.equals("2")) {
                    dao = new ContatoXMLDAO();
                } else {
                    System.out.println("------------ Opcao incorreta !");
                }


            } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
            }
        } while (!resposta.equals("1") && !resposta.equals("2"));
        imprimeCabecalho();
        String nome = lerTeclado();
        while (!nome.equalsIgnoreCase("fim")) {

            if (nome.equals("1")) {
                salvarContato();
            } else if (nome.equals("2")) {
                listarContato();
            } else if (nome.equals("3")) {
                removerContato();
            } else if (nome.equals("4")) {
                adicionarNaveEspacialContato();
            } else if (nome.equals("5")) {
                criarSnapshot();
            } else {
                System.out.println("Digite uma opcao valida");
            }
            imprimeCabecalho();
            nome = lerTeclado();

        }
    }

    private void criarSnapshot() {
        System.out.println("---------------");
        if (!(dao instanceof PrevalenceDAO)) {
            System.out.println("Não é possivel tirar Snapshot, DAO não permite");
            return;
        }
        PrevalenceDAO prevalenceDAO = (PrevalenceDAO) dao;
        prevalenceDAO.takeSnapshot();
        System.out.println("Snapshot criado com sucesso");
    }

    private void adicionarNaveEspacialContato() {
        System.out.println("-------------------" +
                "\n Digite o index do contato: ");
        String index = lerTeclado();
        System.out.println("Digite o nome da nave espacial: ");
        String nomenave = lerTeclado();
        System.out.println("Qual a cor da nave? ");
        String cor = lerTeclado();
        NaveEspacial naveEspacial = new NaveEspacial(nomenave, cor);
        List<Contato> contatos = dao.listar();
        Contato contato = contatos.get(Integer.parseInt(index));
        if (contato != null) {
            dao.remover(contato);
            contato.getNaves().add(naveEspacial);
            dao.adicionar(contato);
            System.out.println("Nave espacial adicionada!");
        }
    }

    private void removerContato() {
        System.out.println("---------------------");
        System.out.println("Digite o index do contato: ");
        String index = lerTeclado();
        List<Contato> contatos = dao.listar();
        Contato contato = contatos.get(Integer.parseInt(index));
        if (contato != null) {
            dao.remover(contato);
            System.out.println("Removido");
        } else {
            System.out.println("Nao localizado!");
        }
    }

    private void imprimeCabecalho() {
        System.out.println("-----------------");
        System.out.println("1) Salvar novo contato ");
        System.out.println("2) Listar contato");
        System.out.println("3) Deletar Contato");
        System.out.println("4) Adicionar um nave espacial ao contato");
        System.out.println("5) Criar um snapshot");
        System.out.print("Digite FIM para sair: ");
    }

    private void salvarContato() {
        Contato contato = new Contato();
        System.out.println();
        System.out.println("Entre com nome do contato: ");
        String dado = lerTeclado();
        contato.setNome(dado);
        System.out.println("Entre com endereco do contato: ");
        dado = lerTeclado();
        contato.setEndereco(dado);
        System.out.println("Entre com telefone do contato: ");
        dado = lerTeclado();
        contato.setTelefone(dado);
        System.out.println("Entre com celular do contato: ");
        dado = lerTeclado();
        contato.setCelular(dado);
        System.out.println("Entre com email do contato: ");
        dado = lerTeclado();
        contato.setEmail(dado);
        dao.adicionar(contato);

    }

    private void listarContato() {
        List<Contato> contatoList = dao.listar();
        int count = 0;
        System.out.println("--------------------");
        for (Contato contato : contatoList) {
            System.out.println(count + " - " + contato);
            count++;
        }
    }

    private String lerTeclado() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                System.in));
        try {
            return reader.readLine();
        } catch (IOException e1) {
            return "FIM";
        }
    }
}
