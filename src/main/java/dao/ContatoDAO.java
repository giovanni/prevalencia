package dao;

import dao.prevayler.AdicionarContato;
import dao.prevayler.ListaContato;
import dao.prevayler.RemoverContato;
import entidades.Contato;
import org.prevayler.Prevayler;
import org.prevayler.PrevaylerFactory;

import java.util.List;

/**
 * Dao para Contatos
 * User: giovanni
 * Date: 15/05/12
 * Time: 16:04
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class ContatoDAO implements DAO<Contato>, PrevalenceDAO {
    protected static Prevayler prevayler;

    public ContatoDAO() throws Exception {
        PrevaylerFactory factory = new PrevaylerFactory();
        factory.configurePrevalenceDirectory("BASE");
        factory.configurePrevalentSystem(new ListaContato());
        prevayler = factory.create();
    }

    @Override
    public List<Contato> listar() {
        ListaContato contatoList = ((ListaContato) prevayler.prevalentSystem());
        return contatoList.getLista();
    }

    @Override
    public void adicionar(Contato entidade) {
        prevayler.execute(new AdicionarContato(entidade));
    }

    @Override
    public void remover(Contato entidade) {
        prevayler.execute(new RemoverContato(entidade));
    }

    @Override
    public void atualizar(Contato entidade) {
        remover(entidade);
        adicionar(entidade);
    }

    @Override
    public void takeSnapshot() {
        try {
            prevayler.takeSnapshot();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
