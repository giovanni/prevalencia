package dao.prevayler;

import entidades.Contato;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Persistir uma lista de contatos
 * User: giovanni
 * Date: 16/05/12
 * Time: 09:57
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class ListaContato implements Serializable {
    private static final long serialVersionUID = 1L;
    private List<Contato> lista = new ArrayList<Contato>();

    public void add(Contato contato) {
        lista.add(contato);
    }

    public Contato get(int i) {
        return lista.get(i);
    }

    public int size() {
        return lista.size();
    }

    public List<Contato> getLista() {
        return lista;
    }

    public void remover(Contato contato) {
        lista.remove(contato);
    }
}
