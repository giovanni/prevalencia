package dao.prevayler;

import entidades.Contato;
import org.prevayler.Transaction;

import java.io.Serializable;
import java.util.Date;

/**
 * Implementa um transação para adicionar contato
 * User: giovanni
 * Date: 16/05/12
 * Time: 10:05
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class AdicionarContato implements Transaction, Serializable {
    private static final long serialVersionUID = 3L;
    private Contato contato;

    public AdicionarContato(Contato contato) {
        this.contato = contato;
    }

    @Override
    public void executeOn(Object o, Date date) {
        ((ListaContato) o).add(contato);
    }
}
