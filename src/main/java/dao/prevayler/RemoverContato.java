package dao.prevayler;

import entidades.Contato;
import org.prevayler.Transaction;

import java.io.Serializable;
import java.util.Date;

/**
 * Implementa uma transação de remoção de contatos
 * User: giovanni
 * Date: 16/05/12
 * Time: 11:05
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class RemoverContato implements Transaction, Serializable {
    private static final long serialVersionUID = 4L;
    private Contato contato;

    public RemoverContato(Contato contato) {
        this.contato = contato;
    }

    @Override
    public void executeOn(Object o, Date date) {
        ((ListaContato) o).remover(contato);
    }
}
