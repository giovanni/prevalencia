package dao;

/**
 * Interface especifica para persistencia usando prevalência
 * User: giovanni
 * Date: 17/05/12
 * Time: 13:52
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public interface PrevalenceDAO {
    public void takeSnapshot();
}
