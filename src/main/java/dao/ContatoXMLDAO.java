package dao;

import dao.prevayler.ListaContato;
import org.prevayler.PrevaylerFactory;
import org.prevayler.foundation.serialization.XStreamSerializer;

/**
 * Proposito do Arquivo
 * User: giovanni
 * Date: 16/05/12
 * Time: 15:04
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public class ContatoXMLDAO extends ContatoDAO {
    public ContatoXMLDAO() throws Exception {
        super();
        PrevaylerFactory factory = new PrevaylerFactory();
        factory.configurePrevalenceDirectory("XML");
        factory.configurePrevalentSystem(new ListaContato());
        factory.configureSnapshotSerializer(new XStreamSerializer());
        prevayler = factory.create();
    }
}
