package dao;

import entidades.Contato;

import java.util.List;
import java.util.Map;

/**
 * Encapsula o acesso ao dado
 * User: giovanni
 * Date: 15/05/12
 * Time: 15:43
 *
 * @author Giovanni Candido da Silva <giovanni@giovannicandido.com>
 */
public interface DAO<T> {
    List<T> listar();
    void adicionar(T entidade);
    void remover(T entidade);
    void atualizar(T entidade);
}
